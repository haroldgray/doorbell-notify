# harold gray
# 17sep2016

# sends notice to kodi instances in the 'hosts' list

import json
from websocket import create_connection

hosts = ['lr-pi', 'br-pi']


def notify_kodi(hostname):
    ws = create_connection("ws://" + hostname + ":9090/jsonrpc")

    # check players on host
    ws.send('{"jsonrpc": "2.0", "id": 1, "method": "Player.GetActivePlayers"}')
    result = json.loads(ws.recv())

    # if Player is active
    if len(result["result"]):
        ws.send('{"jsonrpc": "2.0", "id": 1, "method": "Player.GetProperties",'
                '"params": {"playerid": 1, "properties": ["speed"]}}')
        result = json.loads(ws.recv())

        # and if playing
        if result["result"]["speed"]:
            print "Playback paused on %s" % hostname

            # pause, id:0 is audio player, id:1 is video player
            ws.send('{"jsonrpc": "2.0", "id": 1, "method": "Player.PlayPause", "params": {"playerid": 0}}')
            ws.send('{"jsonrpc": "2.0", "id": 1, "method": "Player.PlayPause", "params": {"playerid": 1}}')

            # this moves time back a few seconds
            ws.send('{"jsonrpc": "2.0", "id": 1, "method": "Player.Seek",'
                    '"params": {"playerid": 1, "value": "smallbackward"}}')
        else:
            print "Playback already paused on %s" % hostname

    else:
        print "There is nothing playing on %s" % hostname

    ws.send('{"jsonrpc": "2.0", "id": 1, "method": "GUI.ShowNotification",'
            '"params": {"title": "******* DING DONG *******", "message": "There is someone at the front door!",'
            '"displaytime": 7500}}')

    ws.close()


def send_notifications():
    for host in hosts:
        notify_kodi(host)


