# harold gray
# 17sep2016

# built for a raspberry pi
# listens for hall effect sensor on GPIO at BCM17

import time
import datetime

import RPi.GPIO as GPIO

import kody
import lighty

print "listening on BCM17"
GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.IN)


def callback(input_pin):
    stamp = time.time()
    stamp = datetime.datetime.fromtimestamp(stamp).strftime('%H:%M:%S')
    print "Doorbell rang at %s" % stamp

    kody.send_notifications()

    lighty.notify()


def main():
    GPIO.add_event_detect(17, GPIO.RISING, callback=callback, bouncetime=5000)

    try:
        while True:
            time.sleep(1)

    except KeyboardInterrupt:
        GPIO.cleanup()


if __name__ == "__main__":
    main()


